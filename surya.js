$(document).ready(function(){
	
	/********************************************************
	 * Initialization section
	 *******************************************************/
	/* load the Google Chart */
	google.charts.load('current', {packages: ['corechart', 'bar']});
	google.charts.setOnLoadCallback(drawBasic);
	
	var plotNumber; //holds the plot number the user selects; will be use to index into the array below
	var house; //holds the house the user selects
	var selectedHouseEnergyValues = new Array(667).fill(0); //array that holds the houses chosen; plot 0 should never be accessed
	var orientation;  //holds the orientation of the plot
	var phase1TotalEnergy = 0;
	var phase3TotalEnergy = 0;
	var phase4TotalEnergy = 0;
	var phase6TotalEnergy = 0;
	var phase7TotalEnergy = 0;
	
	//var totalSolarArray = ;  
        
	var energyAverages = [   //holds the solar energy averages for all Lennar and Brookfield homes
	["Lennar 2258-N",5942.408129],["Lennar 2258-NE",5942.93373],["Lennar 2258-E",5943.206882],["Lennar 2258-SE",5508.638159],
	["Lennar 2258-S",5499.798131],["Lennar 2258-SW",5720.578941],["Lennar 2258-W",5941.091981],["Lennar 2258-NW",5942.042056],
	["Lennar 2528-N",6709.11656],["Lennar 2528-NE",6709.844885],["Lennar 2528-E",6714.577831],["Lennar 2528-SE",6165.38441],
	["Lennar 2528-S",6152.165826],["Lennar 2528-SW",6424.411574],["Lennar 2528-W",6711.435225],["Lennar 2528-NW",6710.870339],
	["Lennar 2526-N",5593.731079],["Lennar 2526-NE",5310.071945],["Lennar 2526-E",4696.357521],["Lennar 2526-SE",5313.813961],
	["Lennar 2526-S",5308.485533],["Lennar 2526-SW",3985.242848],["Lennar 2526-W",4788.882494],["Lennar 2526-NW",5379.128384],
	["Lennar 3031-N",6698.858862],["Lennar 3031-NE",6694.469981],["Lennar 3031-E",6692.136185],["Lennar 3031-SE",6126.934767],
	["Lennar 3031-S",6138.047949],["Lennar 3031-SW",6424.476103],["Lennar 3031-W",6704.262848],["Lennar 3031-NW",6696.652936],
	["Lennar 3058-N",5942.408129],["Lennar 3058-NE",5942.93373],["Lennar 3058-E",5943.206882],["Lennar 3058-SE",5508.638159],
	["Lennar 3058-S",5499.798131],["Lennar 3058-SW",5720.578941],["Lennar 3058-W",5941.091981],["Lennar 3058-NW",5942.042056],
	["Lennar 3585-N",5936.150126],["Lennar 3585-NE",2737.601273],["Lennar 3585-E",4614.305661],["Lennar 3858-SE",5486.702316],
	["Lennar 3585-S",5477.187993],["Lennar 3585-SW",5719.53005],["Lennar 3585-W",5721.387411],["Lennar 3585-NW",4939.519849],
	["Lennar 3640-N",5931.222558],["Lennar 3640-NE",5932.528307],["Lennar 3640-E",5924.08339],["Lennar 3640-SE",5460.737732],
	["Lennar 3640-S",5460.410959],["Lennar 3640-SW",5716.884118],["Lennar 3640-W",5919.423623],["Lennar 3640-NW",5926.519396],
	["Lennar 4155-N",5936.34022],["Lennar 4155-NE",5489.646989],["Lennar 4155-E",4603.291778],["Lennar 4155-SE",5467.666776],
	["Lennar 4155-S",5459.405834],["Lennar 4155-SW",5714.624663],["Lennar 4155-W",4939.519849],["Lennar 4155-NW",5719.78831],
	["Brookfield 1-N",2966.787387],["Brookfield 1-NE",2752.160956],["Brookfield 1-E",2745.81298],["Brookfield 1-SE",1667.468997],
	["Brookfield 1-S",2301.92943],["Brookfield 1-SW",1841.85821],["Brookfield 1-W",1667.468997],["Brookfield 1-NW",2857.068055],
	["Brookfield 2-N",2959.237696],["Brookfield 2-NE",2741.934692],["Brookfield 2-E",2964.484979],["Brookfield 2-SE",2963.472446],
	["Brookfield 2-S",2300.23726],["Brookfield 2-SW",1842.090097],["Brookfield 2-W",1667.468997],["Brookfield 2-W",2856.395182],
	["Brookfield 3-N",2959.167167],["Brookfield 3-NE",2741.830067],["Brookfield 3-E",2964.054165],["Brookfield -SE",2741.210855],
	["Brookfield 3-S",2736.877412],["Brookfield 3-SW",1841.648698],["Brookfield 3-W",1667.468997],["Brookfield 3-NW",2856.303229],
	["Brookfield 4-N",2959.347328],["Brookfield 4-NE",2742.067998],["Brookfield 4-E",2964.200191],["Brookfield 4-SE",2742.360187],
	["Brookfield 4-S",2299.821524],["Brookfield 4-SW",2856.929789],["Brookfield 4-W",1667.468997],["Brookfield 4-NW",2856.308423],
	["Brookfield 5-N",2963.535224],["Brookfield 5-NE",2740.420285],["Brookfield 5-E",1667.468997],["Brookfield 5-SE",2739.026515],
	["Brookfield 5-S",2855.355774],["Brookfield 5-SW",1840.453555],["Brookfield 5-W",1667.468997],["Brookfield 5-NW",2857.375344],
	];
	
	
	/**********************************************************
	 *What happens when each phase, plot, and house is selected
	 **********************************************************/
	//hide yo plots (when the page initially loads)
	$('#p1plots').hide();
	$('#p3plots').hide();
	$('#p4plots').hide();
	$('#p6plots').hide();
	$('#p7plots').hide();
	
	//hide yo house (when the page initially loads)
	$('#lennarhouses').hide();
	$('#brookfieldhouses').hide();
	
	/*When the phases are clicked, show the appropriate plots*/
	$('#phase1').click(function(){
		$('#p1plots').show();  //show plot 1; hide the rest
		$('#p3plots').hide();
		$('#p4plots').hide();
		$('#p6plots').hide();
		$('#p7plots').hide();
		
		$('#lennarhouses').hide();
		$('#brookfieldhouses').hide();
	});
	
	$('#phase3').click(function(){
		$('#p1plots').hide();
		$('#p3plots').show();  //show plot 3; hide the rest
		$('#p4plots').hide();
		$('#p6plots').hide();
		$('#p7plots').hide();
		
		$('#lennarhouses').hide();
		$('#brookfieldhouses').hide();
	});
	
	$('#phase4').click(function(){
		$('#p1plots').hide();
		$('#p3plots').hide();
		$('#p4plots').show();  //show plot 4; hide the rest
		$('#p6plots').hide();
		$('#p7plots').hide();
		
		$('#lennarhouses').hide();
		$('#brookfieldhouses').hide();
	});
	
	$('#phase6').click(function(){
		$('#p1plots').hide();
		$('#p3plots').hide();
		$('#p4plots').hide();
		$('#p6plots').show();  //show plot 6; hide the rest
		$('#p7plots').hide();
		
		$('#lennarhouses').hide();
		$('#brookfieldhouses').hide();
	});
	
	$('#phase7').click(function(){
		$('#p1plots').hide();
		$('#p3plots').hide();
		$('#p4plots').hide();
		$('#p6plots').hide();
		$('#p7plots').show();  //show plot 7; hide the rest
		
		$('#lennarhouses').hide();
		$('#brookfieldhouses').hide();
	});
	
	$('.lennar').click(function(){
		$('#lennarhouses').show(); //show Lennar houses; hide the rest
		$('#brookfieldhouses').hide();
	});
	
	$('.brookfield').click(function(){
		$('#lennarhouses').hide();
		$('#brookfieldhouses').show(); //show Brookfield houses; hide the rest
	});
	
	/********************************************************
	 *The functionality when an house is assigned to a plot 
	 * ******************************************************/
	
	/*if either a lennar or brookfield class is chosen*/
	$('.lennar, .brookfield').click(function(){ 
		orientation = this.getAttribute("data-orientation");  // store the orientation
		plotNumber = this.textContent; //gets the text within; used to determine which plot the user chose
		plotNumber = plotNumber.substring(6, plotNumber.length);  //the '6' is hardcoded to account for "Plot "
		
	});
	
	/*when a house is chosen*/
	$('.house').click(function(){ 
		house = this.textContent; //gets the text within; used to determine which house the user chosen
		
		//index into energyAverages to find the corresponding energy average
		for(var i = 0; i < energyAverages.length; i++)
		{
			var value = energyAverages[i];
			if(value[0] === house)
			{
				//add that value to SelectedHouseEnergyValues
				selectedHouseEnergyValues[plotNumber] = value[1];
			}
		}
		console.log('before the if statement ' + plotNumber + " " + typeof(plotNumber));
		if((6 <= plotNumber && plotNumber <= 10) || (14 <= plotNumber && plotNumber <= 18) || (plotNumber === 25) || (28 <= plotNumber && plotNumber <= 34))   //phase 1
		{
			phase1TotalEnergy = phase1TotalEnergy + selectedHouseEnergyValues[plotNumber];
			console.log('phase 1 ' + plotNumber + " " + typeof(plotNumber));
		}
		else if((162 <= plotNumber && plotNumber <= 179) || (202 <= plotNumber && plotNumber <= 229) || (248 <= plotNumber && plotNumber <= 261))  //phase 3
		{
			phase3TotalEnergy = phase3TotalEnergy + selectedHouseEnergyValues[plotNumber];
			console.log('phase 3 ' + plotNumber + " " + typeof(plotNumber));
		}
		else if((304 <= plotNumber && plotNumber <= 363) || (486 <= plotNumber && plotNumber <= 499) || (510 <= plotNumber && plotNumber <= 514))  //phase 4
		{
			phase4TotalEnergy = phase4TotalEnergy + selectedHouseEnergyValues[plotNumber];
		}
		else if(438 <= plotNumber && plotNumber <= 481)  //phase 6
		{
			phase6TotalEnergy = phase6TotalEnergy + selectedHouseEnergyValues[plotNumber];
		}
		else if((515 <= plotNumber && plotNumber <= 522) || (536 <= plotNumber && plotNumber<= 550) || (563 <= plotNumber && plotNumber<= 577) || (586 <= plotNumber && plotNumber<= 589) || (594 <= plotNumber && plotNumber<= 606))  //phase 7
		{
			phase7TotalEnergy = phase7TotalEnergy + selectedHouseEnergyValues[plotNumber];
		}
		else
		{
			console.log("ERROR.  UNKNOWN PLOT");
		}
	});
	
	/*Show results*/
	$('#viewResults').click(function(){
		drawBasic();
	});
          
	/*******************************************************
 	*Functions
 	*******************************************************/

function drawBasic() {

      var data = new google.visualization.arrayToDataTable([
			['Phase', 'Energy Generated (kW)', { role: 'style' }],
	        ['Phase 1', phase1TotalEnergy, '#006400'],
	        ['Phase 3', phase3TotalEnergy, '#006400'],
	        ['Phase 4', phase4TotalEnergy, '#006400'],
			['Phase 6', phase6TotalEnergy, '#006400' ],
			['Phase 7', phase7TotalEnergy, '#006400' ],
			//['General Homes', 176772.1403,'#006400'],
			//['Small Lennar Homes', 502359.7444, '#006400']
		]);

      var options = {
        title: 'Total Average Energy Generated',
        hAxis: {
          title: 'Development Phase',
        },
        vAxis: {
          title: 'Energy Generated (kW)'
        },
        
        legend: { position: "none" }
      };

      var chart = new google.visualization.ColumnChart(document.getElementById('energy_graph'));
      chart.draw(data, options);
    }


});